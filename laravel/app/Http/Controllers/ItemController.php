<?php

namespace App\Http\Controllers;

use App\Item;
use Grimzy\LaravelCrud\CrudController;

class ItemController extends CrudController
{
    public static $model = Item::class;
    public static $resource = 'item';
}
