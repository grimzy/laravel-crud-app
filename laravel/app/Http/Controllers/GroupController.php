<?php

namespace App\Http\Controllers;

use App\Group;
use Grimzy\LaravelCrud\CrudController;

class GroupController extends CrudController
{
    public static $model = Group::class;
    public static $resource = 'group';
}
