<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Bolded extends AbstractField
{
    protected function getTemplate()
    {
        return 'bolded';
    }
}