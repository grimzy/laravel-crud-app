<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Trimmed extends AbstractField
{
    protected function getTemplate()
    {
        return 'trimmed';
    }
}