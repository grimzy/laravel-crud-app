<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class MultiSelectInput extends AbstractField
{
    protected function getTemplate()
    {
        return 'multi-select-input';
    }

    public function display($result)
    {
        $all_values = [];
        $selected_ids = [];
        if ($this->relation && isset($this->relation['model'])) {
            $relation_items = $this->relation['model']::all();

            if ($relation_items) {
                /** @var \App\Item $item */
                if (method_exists($result, $this->name) && $related = $result->{$this->name}) {
                    $selected_ids = $related->pluck($this->config['relation_id'])->toArray();
                }

                /** @var \App\Group $relation_item */
                foreach ($relation_items as $relation_item) {
                    $all_values[$relation_item->getKey()] = $relation_item->{$this->config['relation_label']};
                }
            }
        };

        return view('crud.fields.'.$this->getTemplate(), compact('item', 'all_values', 'selected_ids'), [
            'resource' => $this->resource,
            'model' => $this->model,
            'field' => $this->name,
        ]);
    }
}