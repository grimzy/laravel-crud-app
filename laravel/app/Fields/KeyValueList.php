<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class KeyValueList extends AbstractField
{
    protected function getTemplate()
    {
        return 'key-value-list';
    }

    public function display($result)
    {
        return view('crud.fields.'.$this->getTemplate(), [
            'values' => $result->{$this->name},
            'resource' => $this->resource,
            'model' => $this->model,
            'field' => $this->name,
        ]);
    }
}