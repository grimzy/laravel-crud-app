<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Plain extends AbstractField
{
    protected function getTemplate()
    {
        return 'plain';
    }

    public function display($result)
    {
        $label = null;
        if($this->config['with_label']) {
            $label = trans('crud.resource.' . $this->resource . '.fields.' . $this->name);
        }

        $value = $result->{$this->name};

        return view('crud.fields.'.$this->getTemplate(), compact('value', 'label'));
    }
}