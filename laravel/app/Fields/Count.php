<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Count extends AbstractField
{
    protected function getTemplate()
    {
        return 'count';
    }

    public function display($result)
    {
        $count = count($result->{$this->name});

        return view('crud.fields.'.$this->getTemplate(), [
            'count' => $count,
            'resource' => $this->resource,
            'field' => $this->name,
            'with_label' => $this->config['with_label']
        ]);
    }
}