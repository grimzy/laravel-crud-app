<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Collection extends AbstractField
{
    protected function getTemplate()
    {
        return 'collection';
    }

    public function display($result)
    {
        return view('crud.fields.'.$this->getTemplate(), [
            'item' => $result,
            'field' => $this->name,
            'resource' => $this->resource,
            'model' => $this->model,
            'id' => $this->config['relation_id'],
            'label' => $this->config['relation_label'],
            'links' => $this->config['links'],
            'with_label' => $this->config['with_label']
        ]);
    }
}