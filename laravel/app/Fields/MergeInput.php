<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class MergeInput extends AbstractField
{
    protected function getTemplate()
    {
        return 'merge-input';
    }

    public function display($result)
    {
        $value = null;
        if($result) {
            $value = $result->{$this->name};
        }

        $string1 = null;
        $string2 = null;

        if ($value) {
            list($string1, $string2) = $this->splitString($value);
        }

        return view('crud.fields.'.$this->getTemplate(), [
            'field' => $this->name,
            'resource' => $this->resource,
            'name1' => "{$this->name}|1",
            'name2' => "{$this->name}|2",
            'string1' => $string1,
            'string2' => $string2,
        ]);
    }

    private function splitString($string)
    {
        $half = (int) ( (strlen($string) / 2) );
        $string1 = substr($string, 0, $half);
        $string2 = substr($string, $half);

        return [$string1, $string2];
    }

    public function setValidation($validation)
    {
        if (is_string($validation)) {
            $this->validation = [
                "{$this->name}|1" => $validation,
                "{$this->name}|2" => $validation,
            ];
        } else {
            $this->validation = $validation;
        }
    }

    public function transformValueFromInput(array $input = [])
    {
        $input_1 = !empty($input["{$this->name}|1"]) ? $input["{$this->name}|1"] : null;
        $input_2 = !empty($input["{$this->name}|2"]) ? $input["{$this->name}|2"] : null;

        return $input_1 . $input_2;
    }
}