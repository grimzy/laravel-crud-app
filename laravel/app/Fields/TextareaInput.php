<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class TextareaInput extends AbstractField
{
    protected function getTemplate()
    {
        return 'textarea-input';
    }
}