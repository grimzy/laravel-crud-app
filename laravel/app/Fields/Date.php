<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class Date extends AbstractField
{
    protected function getTemplate()
    {
        return 'date';
    }

    public function display($result)
    {
        $label = null;
        if($this->config['with_label']) {
            $label = trans('crud.resource.' . $this->resource . '.fields.' . $this->name);
        }

        $date = date($this->config['format'], strtotime($result->{$this->name}));

        return view('crud.fields.'.$this->getTemplate(), compact('date', 'label'));
    }
}