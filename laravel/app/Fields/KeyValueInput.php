<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class KeyValueInput extends AbstractField
{
    protected function getTemplate()
    {
        return 'key-value-input';
    }

    public function display($result)
    {
        $pairs = old($this->name);
        if(!$pairs && isset($result->{$this->name})) {
            $pairs = $result->{$this->name};
        }

        if($pairs) {
            $pairs = json_encode($pairs);
        }

        return view('crud.fields.'.$this->getTemplate(), compact('pairs'), [
            'resource' => $this->resource,
            'field' => $this->name,
        ]);
    }
}