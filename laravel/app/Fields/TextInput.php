<?php

namespace App\Fields;

use Grimzy\LaravelCrud\AbstractField;

class TextInput extends AbstractField
{
    protected function getTemplate()
    {
        return 'text-input';
    }
}