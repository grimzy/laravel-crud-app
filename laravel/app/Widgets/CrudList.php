<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Grimzy\LaravelCrud\Crud;
use Illuminate\Routing\Route;

class CrudList extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     *
     * @param Crud $crud
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run(Crud $crud)
    {
        $items = $crud->getResultsForAction();

        $fields_config = $crud->getFieldsConfigForAction();
        $fields = array_keys(array_filter($fields_config));

        return view("widgets.crud_list", [
            'items' => $items,
            'resource' => $crud->getResource(),
            'fields' => $fields,
            'config' => $this->config
        ]);
    }
}