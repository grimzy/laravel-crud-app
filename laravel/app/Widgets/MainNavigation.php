<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use Grimzy\LaravelCrud\Crud;
use Illuminate\Routing\Route;

class MainNavigation extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     * @param Route $route
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function run(Route $route)
    {
        $items = [
            'home' => ['uri' => 'home', 'label' => 'Home'],
            'item' => ['uri' => 'item', 'label' => 'Items'],
            'group' => ['uri' => 'group', 'label' => 'Groups']
        ];

        foreach ($items as $name => &$item) {
            $item['active'] = ($item['uri'] == $route->uri());
        }

        return view("widgets.main_navigation", [
            'config' => $this->config,
            'items' => $items
        ]);
    }
}