<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $fillable = ['name'];

    /**
     * The items that belong to the group.
     */
    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
