<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['title','description', 'features', 'text'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'features' => 'array',
    ];

    /**
     * The groups that belong to the item.
     */
    public function groups()
    {
        return $this->belongsToMany('App\Group');
    }
}
