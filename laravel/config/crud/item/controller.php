<?php

return [
    'auth' => 'can:view,App\Item',
    'actions' => [
        'index' => [
            'fields' => [
                'id' => [
                    'type' => 'Plain',
                    'config' => [
                        'with_label' => false
                    ]
                ],
                'title' => [
                    'type' => 'Bolded'
                ],
                'description' => [
                    'type' => 'Trimmed'
                ],
                'groups' => [
                    'type' => 'Collection',
                    'config' => [
                        'links' => 'group',
                        'with_label' => false,
                        'relation_id' => 'id',
                        'relation_label' => 'name'
                    ]
                ]
            ],
            'paginate' => 8,
        ],

        'show' => [
            'single_result' => true,
            'fields' => [
                'id' => null,
                'title' => [
                    'type' => 'Plain',
                    'config' => [
                        'with_label' => true
                    ]
                ],
                'description' => [
                    'type' => 'Plain',
                    'config' => [
                        'with_label' => true
                    ]
                ],
                'text' => [
                    'type' => 'plain',
                    'config' => [
                        'with_label' => true
                    ]
                ],
                'groups' => [
                    'type' => 'Collection',
                    'config' => [
                        'links' => 'group',
                        'with_label' => true,
                        'relation_id' => 'id',
                        'relation_label' => 'name'
                    ]
                ],
                'features' => [
                    'type' => 'KeyValueList',
                ],
                'created_at' => [
                    'type' => 'Date',
                    'config' => [
                        'with_label' => true,
                        'format' => 'l, F jS, Y @ h:i a'
                    ]
                ],
                'updated_at' => [
                    'type' => 'Date',
                    'config' => [
                        'with_label' => true,
                        'format' => 'l, F jS, Y @ h:i a'
                    ]
                ],
            ],
        ],

        'create' => [
            'auth' => 'can:create,App\Item',
            'fields' => [
                'title' => [
                    'type' => 'TextInput',
                    'validation' => 'required'
                ],
                'description' => [
                    'type' => 'TextareaInput',
                    'validation' => 'required'
                ],
                'text' => [
                    'type' => 'MergeInput',
                    'validation' => 'required'
                ],
                'groups' => [
                    'type' => 'MultiSelectInput',
                    'config' => [
                        'relation_id' => 'id',
                        'relation_label' => 'name'
                    ]
                ],
                'features' => [
                    'type' => 'KeyValueInput',
                ]
            ]
        ],

        'edit' => [
            'auth' => 'can:edit,App\Item',
            'fields' => [
                'id' => null,
                'title' => [
                    'type' => 'TextInput',
                    'validation' => 'required'
                ],
                'description' => [
                    'type' => 'TextareaInput',
                    'validation' => 'required'
                ],
                'text' => [
                    'type' => 'MergeInput',
                    'validation' => 'required'
                ],
                'groups' => [
                    'type' => 'MultiSelectInput',
                    'config' => [
                        'relation_id' => 'id',
                        'relation_label' => 'name'
                    ]
                ],
                'features' => [
                    'type' => 'KeyValueInput',
                ]
            ]
        ],

        'store' => [
            'action_for' => 'create'
        ],

        'update' => [
            'action_for' => 'edit'
        ]
    ]
];