<?php

return [
    'id' => null,
    'title' => null,
    'groups' => [
        'relation' => [
            'model' => 'App\Group',
            'select' => ['id','name'],
        ],
    ],
    'features' => null,
];