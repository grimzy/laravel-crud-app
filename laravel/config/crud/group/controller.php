<?php

return [
    'auth' => 'can:view,App\Group',
    'actions' => [
        'index' => [
            'fields' => [
                'id' => null,
                'name' => [
                    'type' => 'Bolded'
                ],
                'items' => [
                    'type' => 'Count',
                    'config' => [
                        'with_label' => false
                    ]
                ],
            ],
            'paginate' => 8,
        ],

        'show' => [
            'fields' => [
                'id' => null,
                'name' => [
                    'type' => 'Plain',
                    'config' => [
                        'with_label' => true
                    ]
                ],
                'items' => [
                    'type' => 'Collection',
                    'config' => [
                        'links' => 'item',
                        'with_label' => true,
                        'relation_id' => 'id',
                        'relation_label' => 'title'
                    ]
                ],
                'created_at' => [
                    'type' => 'Date',
                    'config' => [
                        'with_label' => true,
                        'format' => 'l, F jS, Y @ h:i a'
                    ]
                ],
                'updated_at' => [
                    'type' => 'Date',
                    'config' => [
                        'with_label' => true,
                        'format' => 'l, F jS, Y @ h:i a'
                    ]
                ],
            ]
        ],

        'create' => [
            'auth' => 'can:create,App\Group',
            'fields' => [
                'name' => [
                    'type' => 'TextInput',
                    'validation' => 'required'
                ],
                'items' => [
                    'type' => 'MultiSelectInput',
                    'config' => [
                        'relation_id' => 'id',
                        'relation_label' => 'title'
                    ]
                ]
            ]
        ],

        'edit' => [
            'auth' => 'can:edit,App\Group',
            'fields' => [
                'id' => null,
                'name' => [
                    'type' => 'TextInput'
                ],
                'items' => [
                    'type' => 'MultiSelectInput',
                    'config' => [
                        'relation_id' => 'id',
                        'relation_label' => 'title'
                    ]
                ]
            ]
        ],

        'store' => [
            'auth' => 'can:store,App\Group',
            'action_for' => 'create'
        ],

        'update' => [
            'action_for' => 'edit'
        ]
    ]
];