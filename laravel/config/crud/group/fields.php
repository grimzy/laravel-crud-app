<?php

return [
    'id' => null,
    'name' => null,
    'items' => [
        'relation' => [
            'model' => 'App\Item',
            'select' => ['id', 'title'],
        ],
    ],
];