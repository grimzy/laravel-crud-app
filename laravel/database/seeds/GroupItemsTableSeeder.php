<?php

use Illuminate\Database\Seeder;

class GroupItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = factory(App\Item::class, rand(10, 30))->make();

        factory(App\Group::class, rand(3, 6))->create()->each(function($g) use ($items){
            $num = rand(0, $items->count());
            if($num) {
                $related = $items->random($num);
                /** @var $g App\Group */
                $g->items()->saveMany($related);
            }
        });
    }
}
