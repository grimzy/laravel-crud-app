<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'Joe',
            'email' => 'estefanejoe@gmail.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
