@if($items)
    @foreach($items as $item_id => $item)
        <li{{ empty($item['active']) ? '' : ' class=active' }}>
            <a href="{{ url($item['uri']) }}">{{ $item['label'] or '' }}</a>
        </li>
    @endforeach
@endif