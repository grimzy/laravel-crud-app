<table class="table table-bordered">
    <tr>
        @foreach($fields as $field)
            <th>{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}</th>
        @endforeach
        <th>Action</th>
    </tr>

    @foreach ($items as $key => $item)
    <tr>
        @foreach($fields as $field)
            <td>@field($item, $field)</td>
        @endforeach
        <td>
            <div class="btn-toolbar">
                <a class="btn btn-xs btn-info" href="{{ route($resource . '.show',$item->id) }}">Show</a>
                <a class="btn btn-xs btn-primary" href="{{ route($resource . '.edit',$item->id) }}">Edit</a>
                {!! Form::open(['method' => 'DELETE','route' => [$resource . '.destroy', $item->id],'style'=>'display:inline']) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                {!! Form::close() !!}
            </div>
        </td>
    </tr>
    @endforeach
</table>

{!! $items->render() !!}