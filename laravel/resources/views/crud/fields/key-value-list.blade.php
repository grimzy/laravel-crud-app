<label for="{{ $field }}">{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}:</label>
@if($values && is_array($values))
    <ul>
        @foreach($values as $key => $value)
            <li><strong>{{ $key }}</strong>: {{ $value }}</li>
        @endforeach
    </ul>
@endif