<label for="{{ $field }}">{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}:</label>
<div class="row">
    <div class="col-xs-6">
        {!! Form::text($name1, $string1, array('placeholder' => trans('crud.resource.' . $resource . '.fields.' . $field . '_1'), 'id' => "{$field}_1", 'class' => 'form-control')) !!}
    </div>
    <div class="col-xs-6">
        {!! Form::text($name2, $string2, array('placeholder' => trans('crud.resource.' . $resource . '.fields.' . $field . '_2'), 'id' => "{$field}_2", 'class' => 'form-control')) !!}
    </div>
</div>
