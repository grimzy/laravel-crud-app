@if($with_label)
    <strong>{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}:</strong>
@endif
@foreach($item->{$field} as $it)
    <a href="{{ route("{$links}.show", [$it->{$id}]) }}">
        <span class="label label-primary">{{ $it->{$label} }}</span>
    </a>
@endforeach