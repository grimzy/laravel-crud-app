<label for="{{ $field }}">{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}:</label>
<div data-key-value-input data-name="{{ $field }}" data-placeholder-key="{{ trans('fields.key-value-input.key') }}"
     data-placeholder-value="{{ trans('fields.key-value-input.value') }}" data-pairs="{{ $pairs }}"></div>

@push('scripts')
<script type="application/javascript">
    $(function () {
        var $elements = $('[data-key-value-input]');
        if ($elements.length > 0) {
            $.each($elements, function (i, element) {
                var $element = $(element);
                var name = $element.data('name');
                var pairs = $element.data('pairs');

                var $e = $('<div class="col-xs-12">').appendTo($element.addClass('row'));

                // Add the plus button
                var $btn_plus = $('<div class="btn btn-xs btn-info">').text(' Add one more').prepend($('<span class="glyphicon glyphicon-plus">'));

                var toggleEmptyValue = function() {
                    if(!$e.siblings().length) {
                        $e.parent().append($('<input type="hidden" name="' + name + '">'))
                    }
                    else {
                        $e.parent().children('input').remove();
                    }
                };

                var addKeyValueRow = function(key, value) {
                    // Add the key/value
                    var $main_col = $('<div class="col-xs-12" style="margin-bottom: 5px;">');
                    var $new_row = $('<div class="row">');

                    // The hidden field
                    var $hidden = $('<input type="hidden">');

                    // The input fields
                    $.each(['key', 'value'], function (index, part) {
                        var $input = $('<input>', {
                            'class': 'form-control ' + part,
                            'placeholder': $e.data('placeholder-' + part) ? $e.data('placeholder-' + part) : part.charAt(0).toUpperCase() + part.slice(1)
                        });

                        var $input_wrapper = $('<div class="col-xs-5">');
                        $input_wrapper.append($input);
                        $new_row.append($input_wrapper);

                        $input.on('change keyup paste click', function (ev) {
                            var $t = $(ev.target);
                            var val = $t.val();
                            if ($t.hasClass('key')) {
                                if (val) {
                                    $hidden.attr('name', name + '[' + val + ']');
                                }
                                else {
                                    $hidden.removeAttr('name');
                                }
                            }
                            else if ($t.hasClass('value')) {
                                if (val) {
                                    $hidden.val(val);
                                }
                                else {
                                    $hidden.removeAttr('value');
                                }
                            }
                        });

                        if(key && part === 'key') {
                            $input.val(key);
                            $input.trigger('change');
                        }

                        if(value && part === 'value') {
                            $input.val(value);
                            $input.trigger('change');
                        }
                    });

                    // The remove button
                    var $btn_wrapper = $('<div class="form-control-static">');
                    var $btn_minus = $('<div class="btn btn-xs btn-danger">').text(' Remove').prepend($('<span class="glyphicon glyphicon-remove">'));
                    $btn_minus.on('click', function () {
                        $main_col.remove();
                        toggleEmptyValue();
                    });
                    $btn_wrapper.append($btn_minus);
                    $new_row.append($('<div class="col-xs-2">').append($btn_wrapper));

                    // Add the new row to the column and insert it before the add button wrapper
                    $new_row.append($hidden);
                    $main_col.append($new_row);
                    $main_col.insertBefore($e);
                    toggleEmptyValue();
                };

                $btn_plus.on('click', function () {
                    addKeyValueRow();
                });

                $e.append($btn_plus);

                if(pairs) {
                    $.each(pairs, function(key, value) {
                       addKeyValueRow(key, value);
                    });
                }
                else {
                    toggleEmptyValue();
                }
            });
        }
    });
</script>
@endpush