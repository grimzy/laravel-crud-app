@extends('crud.wrapper')

@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            <header class="page-header" style="margin-top: 0;">
                <h1 style="margin-top: 0;">
                    {{ $title }}
                    <div class="btn-toolbar pull-right">
                        <a class="btn btn-default" href="{{ route("{$resource}.index") }}">Back to index</a>
                        <a class="btn btn-primary"
                           href="{{ route("{$resource}.edit", [$item->id]) }}">Edit {{ $resource }}</a>
                    </div>
                </h1>
            </header>
        </div>
    </div>
@stop

@section('content-main')

    <div class="row">
        @foreach($fields as $field)
            <div class="col-xs-12">
                <div class="form-group">
                    @field($field)
                </div>
            </div>
        @endforeach
    </div>

@stop