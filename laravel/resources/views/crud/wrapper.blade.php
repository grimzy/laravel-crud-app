@extends('layouts.app')

@section('title') {{ $title }} @stop

@section('content')
    @yield('content-header')

    @yield('content-main')

    @yield('content-footer')
@stop
