@extends('crud.wrapper')

@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            <header class="page-header" style="margin-top: 0;">
                <h1 style="margin-top: 0;">
                    {{ $title }}
                    <div class="btn-toolbar pull-right">
                        <a class="btn btn-primary" href="{{ URL::route("{$resource}.create") }}">
                            {{ trans('crud.create') . ' ' . trans("crud.resource.{$resource}.name")}}
                        </a>
                    </div>
                </h1>
            </header>
        </div>
    </div>
@stop

@section('content-main')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    {{--Use the widget--}}
    {{--@widget('CrudList')--}}
    {{--Or since the widget only uses the main query, write it here--}}
    <table class="table table-bordered">
        <thead>
        <tr>
            @foreach($fields as $field)
                <th>{{ trans('crud.resource.' . $resource . '.fields.' . $field) }}</th>
            @endforeach
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        @foreach ($items as $key => $item)
            <tr>
                @foreach($fields as $field)
                    <td>@field($field, $item)</td>
                @endforeach
                <td>
                    <div class="btn-toolbar">
                        <a class="btn btn-xs btn-info" href="{{ route($resource . '.show',$item->id) }}">Show</a>
                        <a class="btn btn-xs btn-primary" href="{{ route($resource . '.edit',$item->id) }}">Edit</a>
                        {!! Form::open(['method' => 'DELETE','route' => [$resource . '.destroy', $item->id],'style'=>'margin-left: 5px;float:left;']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn btn-xs btn-danger']) !!}
                        {!! Form::close() !!}
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $items->render() !!}
@stop