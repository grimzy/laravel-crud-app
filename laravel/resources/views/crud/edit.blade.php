@extends('crud.wrapper')

@section('content-header')
    <div class="row">
        <div class="col-lg-12">
            <header class="page-header" style="margin-top: 0;">
                <h1 style="margin-top: 0;">
                    {{ $title }}
                    <div class="btn-toolbar pull-right">
                        <a class="btn btn-default" href="{{ route("{$resource}.index") }}">Back to index</a>
                        <a class="btn btn-info" href="{{ route("{$resource}.show", [$item->id]) }}">Back to {{ $resource }}</a>
                    </div>
                </h1>
            </header>
        </div>
    </div>
@stop

@section('content-main')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::model($item, ['method' => 'PATCH','route' => ["{$resource}.update", $item->id]]) !!}
    <div class="row">

        @foreach($fields as $field)
            <div class="col-xs-12">
                <div class="form-group">
                    @field($field)
                </div>
            </div>
        @endforeach

        <div class="col-xs-12 col-sm-12 col-md-12 text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>

    </div>
    {!! Form::close() !!}

@endsection