<?php

return [

    /*
    |--------------------------------------------------------------------------
    | CRUD Language Lines
    |--------------------------------------------------------------------------
    |
    | TODO: Document
    |
    */

    'create' => 'Create',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'success' => [
        'created' => ':Resource created successfully',
        'updated' => ':Resource updated successfully',
        'deleted' => ':Resource deleted successfully',
    ],

    'resource' => [

        'item' => [
            'name' => 'item',
            'name-plural' => 'items',

            'fields' => [
                'id' => 'ID',
                'title' => 'Title',
                'groups' => 'Groups',
                'description' => 'Description',
                'features' => 'Features',
                'created_at' => 'Created at',
                'updated_at' => 'Updated at',
                'text' => 'Split text',
                'text_1' => 'Text 1',
                'text_2' => 'Text 2'
            ],
        ],

        'group' => [
            'name' => 'group',
            'name-plural' => 'Groups',

            'fields' => [
                'id' => 'ID',
                'name' => 'Name',
                'items' => 'Items',
                'created_at' => 'Created at',
                'updated_at' => 'Updated at'
            ]
        ]
    ]
];
